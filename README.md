# Writing Co.De

[![Netlify Status](https://api.netlify.com/api/v1/badges/8bc9ecd8-e756-4dbc-ac9f-1be874b7be89/deploy-status)](https://app.netlify.com/sites/writingcode/deploys)

## Dependencies

- [VuePress](https://vuepress.vuejs.org/) - Static site generator based with a [Vue.js](https://www.vuejs.org) powered theming system
- [Netlify](https://www.netlify.com/) - Continuous deployment w/ git hooks

## Setup

### Setup Your Own Version

If you like my blog and would like to use the engine powering it, check out [VuePress Blog Boilerplate](https://github.com/bencodezen/vuepress-blog-boilerplate).

### Local Setup

1. Fork, clone or download this project
1. Install dependencies: `npm install` or `yarn`
1. Preview site with: `npm run dev` or `yarn dev`

## Get in Touch

Feel free to reach out to me via the following:

- [Twitter](http://www.twitter.com/autoferrit)
