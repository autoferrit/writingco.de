# About

I am a self taught software developer that loves to build tools and and api's since 2010. I have worked through various startups and larger companies, done consulting, and worked on my own projects. I have used PHP, and JavaScript, but my current passions lie with Python and now learning Rust.

When I got started on this path, I was going to school for 3d modeling and game design. Building levels for friends at school using unreal engine and having lan parties. It was a great time. At some point I started playing with web development when friends and I tried to start our own business. Of course, it failed because we were new to it all. But I quickly fell in love with web design. Building websites and tools for us to try to be more successful entrepreneurs. So I changed my major to CS, went to a local community college (waay less expensive) to take some classes, and loved it. But after a year I found myself moving too fast for what I was getting out of classes. So when some friends and I started working on new ideas, I decided to take a break and never looked back.

Since then I have worked in a variety of environments from small startups, to ones who are successful and growing fast. Now, I am working on my own projects in my free time and couldn't be happier.

## Ask Me Anything (AMA)

If you have any questions for me that I didn't answer here, feel free to [open an issue](https://gitlab.com/autoferrit/writingco.de/issues/new?issue) with the `question` label and I'll do my best to answer it!
