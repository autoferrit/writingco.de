---
home: true
heroImage: /writingcode_logo.webp
heroText: Writing Co.De
tagline: Musing on Programming and Gaming
actionText: Read the latest →
actionLink: /articles/
features:
  - title: Consulting
    details: I provide web development services across the spectrum. From taking design comps and creating a design system with modular & scalable CSS architecture to building single page applications with Vue.js.
  - title: Training & Workshops
    details: I have experience teaching groups of various sizes and skillsets. Whether it's Vue.js, CSS, or some other topic, I would love to share my knowledge with you and your group!
footer: © 2020  Writing Co.De
---


<BlogPostList :list="$site.pages" />
