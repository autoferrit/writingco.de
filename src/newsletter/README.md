# The Writing Co.De Newsletter

I am currently maintaining a weekly newsletter for those who want to keep up with the latest of what I'm up to and the content I'm writing. The emails will come out every tuesday morning.

The newsletter is a work in progress and will definitely change over time since I will be actively listening to feedback and making sure that I provide the most value to you.

## What You'll Get

That said, my plan right now is to provide:

- A list of all posts posted the previous week.
- Nuggets of information to learn how to write code.
- The latest code challenge and the solution from the previous code challenge (if applicable)

## My Promise to You

What I will promise you is that you will **NEVER** get:

- Spam. I loathe it and would never betray your trust in providing me space in your inbox.
- Extraneous updates. When I say weekly, I mean weekly and no more than that. If I ever decide to create a more spontaneous newsletter, I will let people opt into that on their own and will never reuse the emails from the weekly list.

## Subscribe

<MailchimpForm style="margin: 1.5rem 0 3rem;" />
